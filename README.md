# DefocusTracker
Software package for 3D-PTV using the General Defocusing Particle Tracking method.

*DefocusTracker* is based on 3 data structures (MATLAB struct) and 5 functions:
* `imageset`: Link to a collection of images.
* `model`: Data and settings required to run one evaluation.
* `dataset`: Data obtained after one evaluation.  
* `dtracker_create()`: Create data structures.
* `dtracker_show()`: Open GUIs to inspect data structures.
* `dtracker_train()`: Train a model on specific training data.
* `dtracker_process()`: Process an imageset using a given model.
* `dtracker_postprocess()`: Manipulate datasets.

*DefocusTracker* is a modular software. The general workflow remains the same, but different approaches or methods can easily be implemented and applied. 

*DefocusTracker* requires the following MATLAB toolboxes: `curve_fitting_toolbox`, `image_toolbox`, `statistics_toolbox`.

### Installation
1. Download and copy the folder `defocustracker` on your computer.
2. Add `defocustracker` and all the subfolders to your MATLAB path (set path). 
3. Start using *DefocusTracker*!

### Get started
The best and quickest way to learn *DefocusTracker* is by working through examples (WTE). Here how it works:
1. Download the WTE1 script `Work_through_ex1.m` from the folder `workthrough-examples`.
2. Download the WTE1 datasets from [defocustracker.com](https://defocustracking.com/) 
3. Follow the intructions and go through the example.
1. Download the WTE2 script `Work_through_ex2.m` from the folder `workthrough-examples`.
2. Download the WTE2 datasets from [defocustracker.com](https://defocustracking.com/) 
3. Follow the intructions and go through the example.

Now you are ready to use *DefocusTracker* for your application!



