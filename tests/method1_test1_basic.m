%% Test 1: Test method_1 on WTE1 data (duration about 1 minute): 
%  Initialize (this path will work fine if Datesets folder are copied into
%  the 'workthrough-example' folder.

%% Test 1.0: Inizialize 
clc, clear, close all
disp('--------- Test1 STARTED ---------')
path_wte1 = '..\workthrough-examples\Datasets-WTE1';

%% Test 1.1: dtracker_create (imageset, import_ascii, model-method_1)
img_train1 = dtracker_create('imageset',[path_wte1,'//Calib-noise0']);
img_exp1 = dtracker_create('imageset',[path_wte1,'//Data-overlapping-noise0-particles1100']);
dat_true1 = readtable([path_wte1,'//true_coordinates.txt']);
model1 = dtracker_create('model');

%% Test 1.2: dtracker_train
selected_frames = 1:10:501;
N_cal = length(selected_frames);
dat_train1 = dtracker_create('dataset',N_cal);
dat_train1.X(:) = zeros(1,N_cal) + 30;
dat_train1.Y(:) = zeros(1,N_cal) + 30; 
dat_train1.Z(:) = linspace(0,1,N_cal);
dat_train1.fr(:) = selected_frames;
model1.training.imwidth = 51;
model1.training.imheight = 51;
model1 = dtracker_train(model1,img_train1,dat_train1);

%%  Test 1.3: dtracker_process
model1.process.cm_guess = .4;
model1.process.cm_final = .5;
N_frames = 5;
[dat_exp1, time1] = dtracker_process(model1, img_exp1, 1:N_frames);

%%  Test 1.4: dtracker_postprocess (apply_flag, compare_true_values)
flag = dat_true1.fr<=5;
dat_true1 = dat_true1(flag,:);
[errors, errors_delta] = dtracker_postprocess('compare_true_values',...
   dat_exp1,dat_true1);

%% Test 1.5: Summary
disp('          ')
disp('--------- Test1 summary ---------')
disp('parameter = value (reference value*)')
disp('*reference values obtained with defocustracker v.1.0.2 ')
disp('on a laptop with i7-8665U CPU @ 1.90GHz, 16 GB RAM')
disp('--------------------------------')
disp(['Error for x (pixels) = ',num2str(errors.sigma_X,'%0.3f'),' (0.432)'])
disp(['Error for y (pixels) = ',num2str(errors.sigma_Y,'%0.3f'),' (0.435)'])
disp(['Error for z/h = ',num2str(errors.sigma_Z,'%0.3f'),' (0.021)'])
disp(['Recall = ',num2str(errors.recall,'%0.3f'),' (0.330)'])
disp(['Processing time (sec) = ',num2str(time1,'%0.1f'),'(32.2)'])






